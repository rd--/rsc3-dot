(import (rnrs) (mk-r6rs core))

(define at-src
  (lambda (x)
    (string-append "../src/" x ".scm")))

(define rsc3-dot-src
  (map at-src (list "dot" "rsc3-dot")))

(mk-r6rs '(rsc3 dot)
	 rsc3-dot-src
	 (string-append (list-ref (command-line) 1) "/rsc3/dot.sls")
	 '((rnrs) (rhs core) (rsc3 core) (rsc3 server) (rsc3 ugen))
	 '()
	 '())

(exit)
