; ~/.rsc3 ; (import (rsc3 dot))

(draw
 (let* ((o1 (LFSaw '(8 7.23) 0.0))
        (o2 (LFSaw 0.4 0.0))
        (f (MidiCps (MulAdd o2 24 (MulAdd o1 3 80)))))
   (Out 0 (CombN (Mul (SinOsc f 0.0) 0.1) 0.2 0.2 4))))

(draw
 (letc ((freq 440))
   (Out 0 (Mul (SinOsc freq 0) 0.1))))

(draw
 (Out 0 (Pan2 (Mul (SinOsc 440 0) 0.1) (kr: (SinOsc 1 0)) 1)))

; the rsc3 emacs mode includes a key-binding (C-cC-g) to run draw at the current s-expression
(Mul (SinOsc 440 0) 0.1)

;;;; low level

; g=graphdef (not ugen)
(define g (synthdef "g" (letc ((freq 440)) (Out 0 (Mul (SinOsc freq 0) 0.1)))))

; ugen-to-label
(map (lambda (u) (ugen-to-label g u)) (graphdef-ugens g))

; ugen-to-node
(dot-node (ugen-to-node g (graphdef-ugen g 0) 0))

; control-to-node
(dot-node (control-to-node g (graphdef-control g 0) (list-ref (graphdef-defaults g) 0) 0))

; graphdef-to-dot
(dot-digraph (graphdef-to-dot g))
