(define (show t)
  (for-each display (flatten t))
  (newline))

(show (dot-record '((440 0) (sin-osc) (_)))) ; {{440|0}|{sin-osc}|{_}}

(show (dot-attr '((color blue) (shape record)))) ; [color="blue",shape="record"]

(show
 (dot-node
  `(ugen_0 ((shape record)
	    (color blue)
	    (label ,(dot-record `((440 0) (sin-osc) (,(dot-port 'o_0)))))))))

(show (dot-edge '(a (b : 0) ((color red))))) ; a -> b:0 [color="red"];

(show
 (dot-digraph
  '(test ((a ())
	  (b ((color red))))
	 ((a b ((color blue)))
	  (b a ((color green)))))))

(show
 (dot-digraph
  `(test
    ((a ((shape record)
	 (label ,(dot-record `((sin-osc 440 0) (,(dot-port 'o_0)))))))
     (b ((color red))))
    (((a : o_0) b ((color blue)))))))
