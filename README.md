rsc3-dot
--------

[rsc3](http://rohandrape.net/?t=rsc3)
[graph drawing](http://graphviz.org/)
for
[supercollider](http://audiosynth.com/)
UGen graphs

The
[dot](http://www.graphviz.org/doc/info/lang.html)
graph language is specified in:

> Eleftherios Koutsofios and Steven North _Drawing Graphs with
> Dot_, AT&T Technical Report #910904-59113-08TM, 1991.

c.f.
[sc3-dot](https://rohandrape.net/?t=sc3-dot) (2004) and
[hsc3-dot](https://rohandrape.net/?t=hsc3-dot) (2006)

© [rohan drape](http://rohandrape.net), 1998-2022, [gpl](http://gnu.org/copyleft/)
