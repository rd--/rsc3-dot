(define dot-slots
  (lambda (s)
    (list #\{ (intersperse #\| s) #\})))

(define dot-port
  (lambda (n)
    (list "<" n ">")))

(define dot-record
  (lambda (n)
    (dot-slots (map dot-slots n))))

(define dot-attr
  (lambda (a)
    (list #\[
	  (intersperse
	   #\,
	   (map (lambda (l)
		   (let ((key (car l))
			 (value (cadr l)))
		     (list key #\= #\" value #\")) )
		 a))
	  #\])))

(define dot-node
  (lambda (l)
    (let ((n (car l))
	  (a (cadr l)))
      (list n #\space (dot-attr a) #\; #\newline))))

(define dot-edge
  (lambda (e)
    (let ((l (car e))
	  (r (cadr e))
	  (a (caddr e)))
      (list l " -> " r #\space (dot-attr a) #\; #\newline))))

(define dot-digraph
  (lambda (g)
    (let ((name (car g))
	  (n (cadr g))
	  (e (caddr g)))
      (list 'digraph #\space name #\space #\{ #\newline
	    (map dot-node n)
	    (map dot-edge e)
	    #\} #\newline))))
